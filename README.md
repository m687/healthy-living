# Healthy Diet

- [Week One](week-one.md)
- [Week Two](week-two.md)
- [Week Three](week-three.md)
- [Week Four](week-four.md)

# Daily Mantras

1. **I am capable of handling whatever comes my way today.**
2. **I am grateful for this new day and the opportunities it brings.**
3. **I radiate positivity and attract positivity into my life.**
4. **I am worthy of love, success, and happiness.**
5. **I embrace challenges as opportunities for growth and learning.**
6. **I release any negativity from yesterday and embrace the potential of today.**
7. **I am in control of my thoughts and emotions.**
8. **I am filled with energy and vitality, ready to take on the day.**
9. **I approach each task with focus, determination, and enthusiasm.**
10. **I am connected to the universe and aligned with my purpose.**
11. **I attract abundance and prosperity into my life.**
12. **I am kind and compassionate to myself and others.**
13. **I trust the journey of life and let go of resistance.**
14. **I am at peace with my past, present, and future.**
15. **I choose joy, positivity, and gratitude in every moment.**
16. **I am open to receiving and giving love in all its forms.**
17. **I am confident and believe in my abilities.**
18. **I let go of worry and embrace a sense of calm and tranquility.**
19. **I radiate confidence and approach challenges with courage.**
20. **I am the creator of my own reality, and I choose happiness.**
21. **I am a beacon of light, sharing positivity with the world.**
22. **I am mindful and present in each moment, finding beauty in simplicity.**
23. **I release any doubts and trust in the unfolding of my journey.**
24. **I am surrounded by love, protection, and positive energy.**
25. **I am grateful for the lessons of yesterday and excited for the possibilities of today.**

# Weekly Timetable

## Monday:

- **6:00 AM - 6:15 AM:** Wake up and morning stretches
- **6:15 AM - 6:30 AM:** Morning prayer and meditation

**Work:**
- **7:00 AM - 12:00 PM:** Work (5 hours with short breaks)

- **12:00 PM - 12:30 PM:** Lunch break and short walk

**Goals:**
- **12:30 PM - 1:00 PM:** Practice gratitude and journaling
- **1:00 PM - 1:30 PM:** Cultivate positive relationships

- **3:00 PM - 3:15 PM:** Mindful breathing and relaxation

**Exercise and Relaxation:**
- **5:00 PM - 5:45 PM:** Cardio workout or outdoor activity
- **6:00 PM - 6:15 PM:** Light stretching or yoga

- **7:00 PM - 7:15 PM:** Evening prayer and reflection
- **7:30 PM - 8:00 PM:** Quality dinner and family time

- **8:30 PM:** Wind down with a good book

## Tuesday - Friday:

*(Follow a similar structure as Monday)*

- **6:00 AM - 6:15 AM:** Wake up and morning stretches
- **6:15 AM - 6:30 AM:** Morning prayer and meditation

**Work:**
- **7:00 AM - 12:00 PM:** Work (5 hours with short breaks)

- **12:00 PM - 12:30 PM:** Lunch break and short walk

**Goals:**
- **12:30 PM - 1:00 PM:** Mindful living and relaxation

**Exercise and Relaxation:**
- **5:00 PM - 5:45 PM:** Strength training or yoga
- **6:00 PM - 6:15 PM:** Relaxation and deep breathing

- **7:00 PM - 7:15 PM:** Evening prayer and reflection
- **7:30 PM - 8:00 PM:** Nutritious dinner and family time

- **8:30 PM:** Wind down with calming activities

## Saturday:

- **7:00 AM - 7:15 AM:** Wake up and morning stretches
- **7:15 AM - 7:30 AM:** Morning prayer and meditation

**Goals:**
- **8:00 AM - 8:30 AM:** Engage in hobbies and relaxation

**Exercise and Relaxation:**
- **9:00 AM - 10:00 AM:** Outdoor activity or workout
- **10:30 AM - 10:45 AM:** Mindful breathing and reflection

- **12:00 PM - 12:30 PM:** Balanced and nutritious lunch

**Goals:**
- **1:00 PM - 1:30 PM:** Plan future travel and explore

- **3:00 PM - 3:15 PM:** Spread kindness through an act of giving

- **5:00 PM - 5:15 PM:** Laughter and humor activity

- **7:00 PM - 7:15 PM:** Evening prayer and relaxation
- **7:30 PM - 8:00 PM:** Enjoy dinner and leisure time

- **8:30 PM:** Relax with a movie or creative activity

## Sunday:

- **7:00 AM - 7:15 AM:** Wake up and morning stretches
- **7:15 AM - 7:30 AM:** Morning prayer and meditation

**Goals:**
- **8:00 AM - 8:30 AM:** Mindful living and reflection

**Exercise and Relaxation:**
- **9:00 AM - 10:00 AM:** Nature walk and mindfulness
- **10:30 AM - 10:45 AM:** Mindful breathing and relaxation

- **12:00 PM - 12:30 PM:** Nutritious lunch

**Goals:**
- **1:00 PM - 1:30 PM:** Plan for future exploration and travel

- **3:00 PM - 3:15 PM:** Relaxation and self-care

- **5:00 PM - 5:15 PM:** Generosity and community engagement

- **7:00 PM - 7:15 PM:** Evening prayer and reflection
- **7:30 PM - 8:00 PM:** Dinner and quality time with loved ones

- **8:30 PM:** Prepare for a restful night's sleep

# Weekly Goals

## Monday:

### Happy Goals:
- Practice Gratitude: Start a gratitude journal.
- Cultivate Positive Relationships: Reach out to a friend or family member.
- Engage in Hobbies: Spend 30 minutes on a hobby you enjoy.

### Healthy Goals:
- Regular Exercise: Cardio workout for 30 minutes.
- Balanced Nutrition: Plan a nutritious meal with plenty of veggies.
- Adequate Hydration: Drink at least 8 glasses of water.

### Wealthy Goals:
- Financial Planning: Review your budget and savings goals.
- Continuous Learning: Read an article or watch a video on a skill you want to improve.

## Tuesday:

### Happy Goals:
- Mindful Living: Practice 10 minutes of meditation.
- Spread Kindness: Perform a random act of kindness.

### Healthy Goals:
- Quality Sleep: Aim for 7-9 hours of sleep tonight.
- Stress Management: Practice deep breathing exercises.
- Regular Check-ups: Schedule a health check-up if needed.

### Wealthy Goals:
- Save and Invest: Review your investment portfolio.
- Multiple Income Streams: Research a potential side gig.

## Wednesday:

### Happy Goals:
- Travel and Explore: Research a place you'd like to visit.
- Laugh More: Watch a comedy show or movie.

### Healthy Goals:
- Regular Exercise: Strength training workout for 30 minutes.
- Limit Screen Time: Set a timer for breaks from screens.
- Mental Health: Practice gratitude journaling.

### Wealthy Goals:
- Financial Planning: Evaluate your financial goals and adjust as needed.

## Thursday:

### Happy Goals:
- Spread Kindness: Compliment or express gratitude to someone.

### Healthy Goals:
- Balanced Nutrition: Cook a well-balanced dinner.
- Adequate Hydration: Drink herbal tea for relaxation.

### Wealthy Goals:
- Continuous Learning: Take an online course or attend a webinar.

## Friday:

### Happy Goals:
- Focus on Self-Care: Pamper yourself with a relaxing bath.

### Healthy Goals:
- Regular Exercise: Yoga or stretching session.
- Quality Sleep: Create a calming bedtime routine.

### Wealthy Goals:
- Debt Reduction: Make an extra payment towards a high-interest debt.
- Entrepreneurship: Brainstorm business ideas.

## Saturday:

### Happy Goals:
- Engage in Hobbies: Spend time on your favorite hobby.

### Healthy Goals:
- Regular Exercise: Outdoor activity like hiking or biking.
- Balanced Nutrition: Prepare a healthy picnic or meal.

### Wealthy Goals:
- Retirement Planning: Review your retirement accounts and contributions.
- Generosity: Volunteer or donate to a local charity.

## Sunday:

### Happy Goals:
- Mindful Living: Take a nature walk and practice mindfulness.
- Travel and Explore: Plan your next adventure.

### Healthy Goals:
- Quality Sleep: Ensure a restful night's sleep.
- Stress Management: Practice progressive muscle relaxation.

### Wealthy Goals:
- Save and Invest: Research investment options for diversification.
- Generosity: Give back to your community through volunteering.
